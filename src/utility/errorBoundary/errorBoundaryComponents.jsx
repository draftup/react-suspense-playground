// @flow strict

import * as React from "react";

type FallbackViewComponentProps = {|
  error: Error,
|};

type FallbackViewComponent = (props: FallbackViewComponentProps) => React.Node;

type Props = {|
  children: React.Node,
  fallbackComponent: FallbackViewComponent,
  errorConstructorsToCatch: Class<Error>[],
|};

type State = {|
  error: null | Error,
|};

export class ErrorBoundaryComponent extends React.Component<Props, State> {
  state: State = {
    error: null,
  };

  static getDerivedStateFromError(error: Error): State {
    return {
      error,
    };
  }

  render(): React.Node {
    const {
      children,
      fallbackComponent: FallbackComponent,
      errorConstructorsToCatch,
    } = this.props;

    const { error } = this.state;

    const matchFound = errorConstructorsToCatch.some(
      (Constructor) => error instanceof Constructor
    );

    if (error && matchFound) {
      return <FallbackComponent error={error} />;
    }

    if (error && !matchFound) {
      throw error;
    }

    return children;
  }
}
