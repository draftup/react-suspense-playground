// @flow

import { Cache } from "@draftup/utility.js";

export type AsyncCacheReaderParams = {
  staleIsFine: boolean,
};

const defaulReaderParams: AsyncCacheReaderParams = {
  staleIsFine: false,
};

export class AsyncCache<ValueType> extends Cache<ValueType> {
  #service: () => Promise<ValueType>;

  #process: Promise<void> | Error | void;

  constructor(service: () => Promise<ValueType>) {
    super();

    this.#service = service;
  }

  #actualizeProcess(): void {
    const invalidated = this.isInvalidated();

    const updating = this.#process instanceof Promise;

    if (invalidated && !updating) {
      this.#process = this.#service()
        .then((nextValue) => {
          this.#process = undefined;

          this.updateValue(nextValue);
        })
        .catch((error) => {
          this.#process = error;
        });
    }
  }

  read(
    params: AsyncCacheReaderParams = defaulReaderParams
  ): Promise<void> | Error | ValueType | void {
    const { staleIsFine } = params;

    this.#actualizeProcess();

    if (this.#process instanceof Error) {
      return this.#process;
    }

    if (this.#process instanceof Promise && !staleIsFine) {
      return this.#process;
    }

    return this.getValue();
  }
}
