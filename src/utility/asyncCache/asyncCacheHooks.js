// @flow

import { useSyncExternalStore } from "react";
import type { AsyncCache, AsyncCacheReaderParams } from "./asyncCacheUtils";

export function useAsyncCacheValue<ValueType>(
  asyncCache: AsyncCache<ValueType>,
  params?: AsyncCacheReaderParams
): ValueType {
  const value = useSyncExternalStore(
    // $FlowIgnore
    (callback) => asyncCache.subscribe(callback),
    () => {
      const cursor = (readerParams?: AsyncCacheReaderParams) => {
        const nextValue = asyncCache.read(readerParams);

        if (nextValue === undefined) {
          asyncCache.invalidate();

          return cursor();
        }

        return nextValue;
      };

      return cursor(params);
    }
  );

  if (value instanceof Promise || value instanceof Error) {
    throw value;
  }

  return value;
}
