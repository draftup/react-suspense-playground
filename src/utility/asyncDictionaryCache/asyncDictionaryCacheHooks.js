// @flow

import { useSyncExternalStore } from "react";
import type {
  AsyncDictionaryCache,
  AsyncDictionaryCacheReaderParams,
} from "./asyncDictionaryCacheUtils";

export function useAsyncDictionaryCacheValue<KeyType, ValueType>(
  asyncDictionaryCache: AsyncDictionaryCache<KeyType, ValueType>,
  key: KeyType,
  params?: AsyncDictionaryCacheReaderParams
): ValueType {
  const value = useSyncExternalStore(
    // $FlowIgnore
    (callback) => asyncDictionaryCache.subscribe(callback),
    () => {
      const cursor = (readerParams?: AsyncDictionaryCacheReaderParams) => {
        const nextValue = asyncDictionaryCache.read(key, readerParams);

        if (nextValue === undefined) {
          asyncDictionaryCache.invalidate(key);

          return cursor();
        }

        return nextValue;
      };

      return cursor(params);
    }
  );

  if (value instanceof Promise || value instanceof Error) {
    throw value;
  }

  return value;
}
