// @flow

import { DictionaryCache } from "@draftup/utility.js";

export type AsyncDictionaryCacheReaderParams = {
  staleIsFine: boolean,
};

const defaulReaderParams: AsyncDictionaryCacheReaderParams = {
  staleIsFine: false,
};

export class AsyncDictionaryCache<KeyType, ValueType> extends DictionaryCache<
  KeyType,
  Promise<void> | Error | ValueType
> {
  #service: (key: KeyType) => Promise<ValueType>;

  #processMap: Map<KeyType, Promise<void> | Error> = new Map();

  constructor(service: (key: KeyType) => Promise<ValueType>) {
    super();

    this.#service = service;
  }

  #actualizeProcess(key: KeyType) {
    const invalidated = this.isInvalidated(key);

    const updating = this.#processMap.get(key) instanceof Promise;

    if (invalidated && !updating) {
      const promise = this.#service(key)
        .then((nextValue) => {
          this.updateRecord(key, nextValue);

          this.#processMap.delete(key);
        })
        .catch((error) => {
          this.#processMap.set(key, error);
        });

      this.#processMap.set(key, promise);
    }
  }

  read(
    key: KeyType,
    params?: AsyncDictionaryCacheReaderParams = defaulReaderParams
  ): Promise<void> | Error | ValueType | void {
    const { staleIsFine } = params;

    this.#actualizeProcess(key);

    const process = this.#processMap.get(key);

    if (process instanceof Error) {
      return process;
    }

    if (process instanceof Promise && !staleIsFine) {
      return process;
    }

    return this.getValue(key);
  }
}
