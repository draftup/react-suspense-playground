/* eslint-disable max-classes-per-file */
// @flow

export class Article {
  title: string;

  description: string;

  constructor(title: string, description: string) {
    this.title = title;
    this.description = description;
  }
}

export class ArticleNotFoundError extends Error {
  name: string = "Article Not Found";

  constructor(id: string) {
    super();

    this.message = `Article id ${id}`;
  }
}
