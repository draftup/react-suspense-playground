// @flow

import { AsyncTimeout } from "@draftup/utility.js";
import { Article, ArticleNotFoundError } from "./articleUtils";

let latestId = 0;

function createId(): number {
  latestId += 1;

  return latestId;
}

const map: Map<string, Article> = new Map();

map.set(
  `${createId()}`,
  new Article(
    "Зачем тараканам усы?",
    "Многие не задумывается над тем почему у пельменей такой короткий срок жизни..."
  )
);

map.set(
  `${createId()}`,
  new Article(
    "Вот почему елки не падают с неба...",
    "Когда последний скуф играет в черный клуб и пытается забрать всё с собой..."
  )
);

const asyncTimeout = new AsyncTimeout(500);

export async function getArticleIds(): Promise<string[]> {
  await asyncTimeout.start();

  return Array.from(map.keys());
}

export async function getArticle(id: string): Promise<Article> {
  await asyncTimeout.start();

  const article = map.get(id);

  if (!article) {
    throw new ArticleNotFoundError(id);
  }

  return article;
}

export async function postArticle(article: Article): Promise<string> {
  const id = createId();

  map.set(`${id}`, article);

  return `${id}`;
}
