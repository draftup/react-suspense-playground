// @flow

import type { Article } from "./articleUtils";

import { AsyncCache } from "../../utility/asyncCache/asyncCacheUtils";
import { AsyncDictionaryCache } from "../../utility/asyncDictionaryCache/asyncDictionaryCacheUtils";
import * as articleServices from "./articleServices";

export const articleIdsCache: AsyncCache<string[]> = new AsyncCache(() =>
  articleServices.getArticleIds()
);

export const articlesCache: AsyncDictionaryCache<string, Article> =
  new AsyncDictionaryCache((id) => articleServices.getArticle(id));
