// @flow

import type { Article } from "./articleUtils";

import { useAsyncCacheValue } from "../../utility/asyncCache/asyncCacheHooks";
import { useAsyncDictionaryCacheValue } from "../../utility/asyncDictionaryCache/asyncDictionaryCacheHooks";
import { articleIdsCache, articlesCache } from "./articleCaches";

export function useArticleIds(): string[] {
  const value = useAsyncCacheValue(articleIdsCache, { staleIsFine: true });

  return value;
}

export function useArticle(id: string): Article {
  const value = useAsyncDictionaryCacheValue(articlesCache, id, {
    staleIsFine: true,
  });

  return value;
}
