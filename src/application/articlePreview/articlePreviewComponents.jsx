// @flow

import * as React from "react";
import { useArticle } from "../article/articleHooks";

type Props = {
  articleId: string,
};

export const ArticlePreviewComponent: React.StatelessFunctionalComponent<Props> =
  (props) => {
    const { articleId } = props;

    const article = useArticle(articleId);

    return (
      <div>
        <h2>{article.title}</h2>
        <p>{article.description}</p>
      </div>
    );
  };
