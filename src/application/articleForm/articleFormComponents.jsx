// @flow

import * as React from "react";
import { useAsyncProcessWrapper } from "@draftup/utility.react";
import { Article } from "../article/articleUtils";
import * as articleServices from "../article/articleServices";
import { articleIdsCache } from "../article/articleCaches";

type Props = {};

export const ArticleFormComponent: React.StatelessFunctionalComponent<Props> =
  () => {
    const asyncProcessWrapper = useAsyncProcessWrapper();

    const submit = React.useCallback(
      (event: SyntheticEvent<HTMLFormElement>) => {
        event.preventDefault();

        const formData = new FormData(event.currentTarget);

        const title = formData.get("title") || "";
        const description = formData.get("description") || "";

        const article = new Article("", "");

        if (typeof title === "string") {
          article.title = title;
        }

        if (typeof description === "string") {
          article.description = description;
        }

        asyncProcessWrapper.wrap(
          articleServices.postArticle(article).then(() => {
            articleIdsCache.invalidate();
          })
        );
      },
      [asyncProcessWrapper]
    );

    const pending = asyncProcessWrapper.process.stage.name === "PENDING";

    return (
      <form onSubmit={submit}>
        <input
          name="title"
          type="text"
          placeholder="Заголовок"
          disabled={pending}
        />
        <br />
        <textarea
          name="description"
          placeholder="Описание"
          disabled={pending}
        />
        <br />
        <input type="submit" disabled={pending} />
      </form>
    );
  };
