// @flow

import * as React from "react";
import { useArticleIds } from "../article/articleHooks";
import { ArticlePreviewComponent } from "../articlePreview/articlePreviewComponents";
import { ArticleFormComponent } from "../articleForm/articleFormComponents";

type Props = {};

export const MainPageComponent: React.StatelessFunctionalComponent<Props> =
  () => {
    const articleIds = useArticleIds();

    return (
      <div>
        <h1>Последние статьи:</h1>
        {articleIds.map((articleId) => (
          <React.Suspense key={articleId} fallback={<h2>Wait...</h2>}>
            <ArticlePreviewComponent articleId={articleId} />
          </React.Suspense>
        ))}
        <h1>Добавить статью:</h1>
        <ArticleFormComponent />
      </div>
    );
  };
