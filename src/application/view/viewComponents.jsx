// @flow

import * as React from "react";
import { ErrorBoundaryComponent } from "../../utility/errorBoundary/errorBoundaryComponents";
import { MainPageComponent } from "../mainPage/mainPageComponents";

type FallbackProps = {
  error: Error,
};

const FallbackComponent: React.StatelessFunctionalComponent<FallbackProps> = ({
  error,
}) => <h1>{error.message}</h1>;

type Props = {};

export const ViewComponent: React.StatelessFunctionalComponent<Props> = () => (
  <ErrorBoundaryComponent
    fallbackComponent={FallbackComponent}
    errorConstructorsToCatch={[Error]}
  >
    <React.Suspense fallback={<h1>Wait...</h1>}>
      <MainPageComponent />
    </React.Suspense>
  </ErrorBoundaryComponent>
);
