// @flow

import ReactDOM from "react-dom/client";

import { ViewComponent } from "./application/view/viewComponents";

const rootNode = document.getElementById("root");

const root = ReactDOM.createRoot(rootNode);

root.render(<ViewComponent />);
